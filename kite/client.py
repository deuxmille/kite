import requests

class NeoCities:
	def init(self, username = None, password = None):
		self.auth = (username, password)
		self.url = 'https://neocities.org'

	def info(self, sitename = ''):
		if sitename:
			args = {'sitename': sitename}
		else:
			args = None
		response = requests.get(self._request_url('info'), auth=self.auth, params=args)
		return self._decode(response)

	def delete(self, *filenames):
		args = {'filenames[]': []}
		for i in filenames:
			args['filenames[]'].append(i)
		response = requests.post(self._request_url('delete'), auth = self.auth, data = args)
		return self._decode(response)

	def upload(self, *filenames):
		args = {i[0]: open(i[1], 'rb') for i in filenames}
		response = requests.post(self._request_url('upload'),auth=self.auth,files=args)
		return self._decode(response)

	def list(self, path = ''):
		if path:
			args = {'path': path}
		else:
			args = None
		response = requests.get(self._request_url('list'),auth=self.auth,params=args)
		return self._decode(response)

	def _request_url(self, api):
		return '{0}/api/{1}'.format(self.url,api)

	def _decode(self, response):
		if response.status_code != 200:
			raise NeoCities.InvalidRequest(response.status_code)
		else:
			return response.json()

	class InvalidRequest(Exception):
		def __init__(self, statuscode):
			self.statuscode = statuscode

		def __str__(self):
			return "Request returned status code {}".format(self.statuscode)

