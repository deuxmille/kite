## Kite
# What is Kite?
Kite is a Neocities CLI and client made in Python and designed to work integrated with Git.
# Why is it special?
Because thanks to the Git integration, Kite can choose which files to upload by checking which files are changed and/or (nor xor) untracked, reducing the bandwidth usage
# How can I install it?
Just follow this easy steps:
1. Clone this repository
2. Switch to the repository's directory
3. Run `python3 setup.py install`
4. Done!
# How do I use this!?!? D:
Relax, relax.
In the first run, it may ask you for credentials, just enter them (we won't steal them)
1. Create a directory which will contain your site
2. Switch to it.
3. Run `kite new`
4. Put every file to upload in the `site` directory
5. Run `kite push`
# I found a bug!
Please, report it in the issues tracker of this repository.
