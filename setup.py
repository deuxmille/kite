from setuptools import setup

setup(
	name = 'kite-cli',
	version = 'b0.1',
	description = 'A Neocities CLI made in Python with Git integration',
	url = 'https://bitbucket.org/deuxmille/kite',
	author = '(deuxmille)',
	license = 'MIT',
	packages = [
		'kite'
	],
	install_requires[
		'click',
		'GitPython'
	],
	dependency_links = [
		'https://github.com/neocities/python-neocities'
	],
	entry_points = {
		'kite': ['kite.CLI:main']
	}
)
